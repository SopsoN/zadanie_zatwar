using System;
using System.Collections;
using System.Data;
using System.Data.Common;


namespace zadanie_dwa
{
    class TriangleClass : FigureClass, IFigures
    {
        protected double height;
        double tri_base;

        public TriangleClass()
        {
        }

        public TriangleClass(double h)
        {
            this.height = h;
        }

        public TriangleClass(TriangleClass tri)
        {
            this.height = tri.height;
        }

        public void get_params(ref double h)
        {
            Console.WriteLine("Podaj promien okregu.");

            try
            {
                Console.WriteLine("Wproawdz tylko cyfry.");
                this.height = double.Parse(Console.ReadLine());
                h = height;
                tri_base = 2*h-1;
            }
            catch(Exception e)
            {
                Console.WriteLine("Wartosc, ktora zostala podan jest nieprawidlowa.");
                Console.WriteLine("Sprobuj ponownie...");
                Console.WriteLine("Narysuje trojkat z domyslna wysokoscia" + h.ToString());
                this.height = h;
                tri_base = 2*h-1;
            }
        } 

        public override void draw(out double field)
        {   
            moveToY();

            for (int i = 1; i <= height; i++)
            {
                moveToX();

                for (int j = 1; j <= 2*i-1; j++)
                {
                    Console.Write("*");
                    tri_base = j;
                }

                Console.Write("\n");
            }

            field = (tri_base*height)/2;
        }

        public void draw(double eventHeight, double window_field)
        {   
            if( window_field > 10)
            {
                moveToY();

                for (int i = 1; i <= eventHeight; i++)
                {
                    moveToX();

                    for (int j = 1; j <= 2*i-1; j++)
                    {
                        Console.Write("*");
                    }

                    Console.Write("\n");
                }
            }
        }

        public double get_figure_field()
        {
            return (tri_base*height)/2;
        }

        public static bool operator <(TriangleClass tri1, TriangleClass tri2)
        {
            return (tri1.height < tri2.height);
        }

        public static bool operator >(TriangleClass tri1, TriangleClass tri2)
        {
            return (tri1.height > tri2.height);
        }
    }
}