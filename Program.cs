﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;

namespace zadanie_dwa
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuController menu = new MenuController();

            do
            {
                menu.draw_menu();
                menu.ask_repeat();
            }
            while(menu.again);
        }
    }
}
