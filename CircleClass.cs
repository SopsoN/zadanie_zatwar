using System;
using System.Collections;
using System.Data;
using System.Data.Common;

namespace zadanie_dwa
{
    class CircleClass : FigureClass, IFigures
    {
        private double r;

        public CircleClass()
        {
        }

        public CircleClass(double r)
        {
            this.r = r;
        }

        public CircleClass(CircleClass cir)
        {
            this.r = cir.r;
        }

        public double Radius
        {
            set
            {
                if(value > 0) 
                    this.r = value;
                else
                {
                    this.r = 7;
                    Console.WriteLine("Wartosc jest niepoprawna, domyslna zostala uzyta.\n Wartosc domyslna: "+this.r);
                }
            }

            get
            {
                return this.r;
            }
        }

        public void get_params()
        {
            Console.WriteLine("Podaj promien okregu.");
            Console.WriteLine("Wproawdz tylko cyfry.");

            try
            {
                r = double.Parse(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine("Wartosc, ktora zostala podan jest nieprawidlowa.");
                Console.WriteLine("Sprobuj ponownie...");

                get_params();
            }
        }
        public override void draw(out double field)
        {
            double circleRadius = r*r;
            double y = -r; 
            
            moveToY();

            do
            {
                moveToX();

                for(double x = -r; x <= r; x++)
                {
                    double compare_radius  = y*y + x*x;

                    if(compare_radius <= circleRadius)
                        Console.Write(" *");
                    else
                        Console.Write("  ");
                }

                Console.WriteLine();
                y++;

            }while(r >= y);

            field = Math.PI*r*r;
        } 

        public int retrunRoundRadiusFromField(double field)
        {
            return Convert.ToInt16(Math.Round(Math.Sqrt(field)/Math.PI));
        }

        public double get_figure_field()
        {
            return Math.PI*r*r;;
        }
    }
}