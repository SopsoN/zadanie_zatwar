using System;
using System.Collections;
using System.Data;
using System.Data.Common;


namespace zadanie_dwa
{
    class RectangleClass : FigureClass, IFigures
    {
        private double a;
        private double b;

        public RectangleClass()
        {
        }

        public RectangleClass(double a, double b)
        {
            this.a = a;
            this.b = b;
        }

        public RectangleClass(RectangleClass rect)
        {
            this.a = rect.a;
            this.b = rect.b;
        }

        public double Base
        {
            set 
            {
                if(value > 0)
                    this.a = value;
                else
                {
                    this.a = 10;
                    Console.WriteLine("Wartosc jest niepoprawna, domyslna zostala uzyta.\n Wartosc domyslna: "+this.a);
                }
            }

            get 
            {
                return this.a;
            }
        }

        public double Height
        {
            set
            {
                if(value > 0)
                    this.b = value;
                else
                {
                    this.b = 12;
                    Console.WriteLine("Wartosc jest niepoprawna, domyslna zostala uzyta.\n Wartosc domyslna: "+this.b);
                }
            }

            get
            {
                return this.b;
            }
        }

        public void get_params()
        {
            Console.WriteLine("Podaj pierwszy bok/szerokosc prostokata.");
            Console.WriteLine("Wproawdz tylko cyfry.");

            try
            {
                this.Base = double.Parse(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine("Wartosc, ktora zostala podan jest nieprawidlowa.");
                Console.WriteLine("Sprobuj ponownie...");

                get_params();
            }

            Console.WriteLine("Podaj drugi bok/wysokosc prostokata.");
            Console.WriteLine("Wproawdz tylko cyfry, a czesc dziesietna oddziel kropka.");

            try
            {
                this.Height = double.Parse(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine("Wartosc, ktora zostala podan jest nieprawidlowa.");
                Console.WriteLine("Sprobuj ponownie...");

                get_params();
            }
        }
        public override void draw(out double field)
        {
            moveToY();

            for(int i = 0; i < b; i++)
            {
                moveToX();

                if(i == 0 || i == (b-1))
                {
                    for(int j = 0; j < a; j++)
                        Console.Write("*");
                }
                else
                {
                    for(int j = 0; j < a; j++)
                    {
                        if( j == 0 || j == (a-1))
                            Console.Write("*");
                        else
                            Console.Write(" ");
                    }
                }

                Console.Write("\n");
            }

            field = a * b;
        } 

        public void draw(double drawSameSides, double window_field)
        {
            if( window_field <= 10)
            {
                moveToY();

                for(int i = 0; i < drawSameSides; i++)
                {
                    moveToX();

                    if(i == 0 || i == (drawSameSides-1))
                    {
                        for(int j = 0; j < drawSameSides; j++)
                            Console.Write("*");
                    }
                    else
                    {
                        for(int j = 0; j < drawSameSides; j++)
                        {
                            if( j == 0 || j == (drawSameSides-1))
                                Console.Write("*");
                            else
                                Console.Write(" ");
                        }
                    }

                    Console.Write("\n");
                }
            }
        }

        public double get_figure_field()
        {
            return a*b;
        }

        public static RectangleClass operator +(RectangleClass rect1, RectangleClass rect2)
        {
            double new_base = rect1.Base + rect2.Base;
            double new_height = rect1.Height + rect2.Height;
            return new RectangleClass(new_base, new_height);
        }
    }
}