using System;
using System.Collections;
using System.Data;
using System.Data.Common;


namespace zadanie_dwa
{
    public class MenuController
    {
        static int repeat = 0;
        public bool again = true;
        private int baseFigurePosX;
        private int baseFigurePosY;


        public MenuController()
        {
        }

        public void draw_menu()
        {
            Console.Clear();
            MenuController.repeat++;

            Console.WriteLine("To menu wyswietla sie " + MenuController.repeat.ToString() + " raz.");
            Console.WriteLine("Witaj! \nCo chcesz zeby zostalo narysowane?");
            Console.WriteLine("1. Prostokat\n2. Trojkat\n3. Kolo\n4. Dodaj dwa prosotkaty\n5. Porownaj dwa trojkaty");
            Console.WriteLine("6. Ustaw pozycje figur\n7. Rysowanie kola ze \"Zdarzeniem\"");

            get_answer();
        }

        private void get_answer()
        {
            Console.WriteLine("Wprowadz cyfre odpowiadajaca figurze ktora chcesz narysowac:");
            
            String answer = "";
            
            int answer_int = 0;
            
            answer = Console.ReadLine();

            try
            {    
                answer_int = int.Parse(answer);
            }
            catch
            {
                draw_menu();
            }

            double field = 0;

            switch(answer_int)
            {
                case 1:
                {
                    RectangleClass figure = new RectangleClass();
                    figure.get_params();
                    figure.setPosXY(baseFigurePosX, baseFigurePosY);

                    Console.Clear();
                    figure.draw(out field);
                    break;
                }
                case 2:
                {
                    double tri_height = 12;
                    
                    Console.WriteLine("Domyslna wysokosc trojkata to " + tri_height.ToString());
                    
                    TriangleClass figure = new TriangleClass();
                    
                    figure.get_params(ref tri_height);
                    figure.setPosXY(baseFigurePosX, baseFigurePosY);
                    
                    Console.WriteLine("Obecna wysokosc: " + tri_height.ToString());
                    Console.Clear();
                    figure.draw(out field);
                    break;
                }
                case 3:
                {
                    CircleClass figure = new CircleClass();
                    figure.get_params();
                    figure.setPosXY(baseFigurePosX, baseFigurePosY);

                    Console.Clear();
                    figure.draw(out field);
                    break;
                }
                case 4:
                {
                    Console.WriteLine("Tworzenie pierwszego prostokata");
                    RectangleClass rect1 = new RectangleClass();
                    rect1.get_params();

                    Console.WriteLine("Tworzenie drugiego prostokata");
                    RectangleClass rect2 = new RectangleClass();
                    rect2.get_params();

                    RectangleClass rect_sum = rect1+rect2;

                    rect_sum.draw(out field);

                    break;
                }
                case 5:
                {
                    double tri1_h = 12;
                    
                    Console.WriteLine("Tworzenie pierwszego trojkata");
                    Console.WriteLine("Domyslna wysokosc pierwszego trojkata: "+tri1_h);

                    TriangleClass tri1 = new TriangleClass();
                    tri1.get_params(ref tri1_h);
                    
                    Console.WriteLine("Nadana wysokosc to: "+tri1_h);

                    double tri2_h = 9;
                    
                    Console.WriteLine("Tworzenie drugiego trojkata");
                    Console.WriteLine("Domyslna wysokosc drugiego trojkata:  "+tri2_h);
                    
                    TriangleClass tri2 = new TriangleClass();
                    tri2.get_params(ref tri2_h);

                    Console.WriteLine("Nadana wysokosc to: "+tri1_h);

                    if(tri1 > tri2)
                        Console.WriteLine("Pierwszy trojkat jest wiekszy");
                    else
                        Console.WriteLine("Drugi trojkat jest wiekszy");

                    break;
                }
                case 6:
                {
                    Console.WriteLine("Ustalenie pozycji dla wszystkich figur");
                   
                    Console.WriteLine("Podaj odleglosc od lewej krawedzi: ");
                   
                    int new_pos_x = 0;

                    try
                    {
                        new_pos_x = int.Parse(Console.ReadLine());
                        baseFigurePosX = new_pos_x;
                    }
                    catch
                    {
                        Console.WriteLine("Wprowadzono nie poprawna wartosc.");
                        baseFigurePosX= 0;
                    }

                    Console.WriteLine("Podaj odleglosc od gornej krawedzi: ");
                   
                    int new_pos_y = 0;
                   
                    try
                    {
                        new_pos_y = int.Parse(Console.ReadLine());
                        baseFigurePosY = new_pos_y;
                    }
                    catch
                    {
                        Console.WriteLine("Wprowadzono nie poprawna wartosc.");
                        baseFigurePosY = 0;
                    }
                    
                    break;
                }
                case 7:
                {
                    EventClass eventTODO = new EventClass();
                    RectangleClass rect = new RectangleClass();
                    TriangleClass tri = new TriangleClass();

                    eventTODO.otherDraw += rect.draw;
                    eventTODO.otherDraw += tri.draw;

                    Console.Clear();
                    double circle_field;

                    CircleClass circleFigure = new CircleClass();
                    circleFigure.get_params();

                    circleFigure.draw(out circle_field);
                    
                    double window_field = MenuController.get_window_width()*MenuController.get_window_height();
                    window_field = ((circle_field*100)/window_field);

                    eventTODO.setNewPos += rect.setPosXY;
                    eventTODO.setNewPos += tri.setPosXY;

                    int newpos = circleFigure.retrunRoundRadiusFromField(circle_field)*2+1;
                    eventTODO.giveNewPosToFigures(newpos, 0);

                    Console.Clear();

                    int side_val = 0;
                    
                    if( window_field <= 10)
                        side_val = 10;
                    else
                        side_val = 30;

                    eventTODO.DrawEventFigure(side_val, window_field);

                    break;
                }
                default:
                {
                    draw_menu();
                    break;
                }
            }

            try
            {
                Console.WriteLine("Pole figury wyniosi: " + field.ToString() );
            }
            catch(NullReferenceException)
            {
                Console.WriteLine("W tym punkcie nie ma dodanego liczenia pola figury");
            }
        }

        public void ask_repeat()
        {
            Console.WriteLine("Chcesz wlaczyc menu?");
            Console.WriteLine("1. TAK\n2. Wyjdz z programu");
            
            string answer = Console.ReadLine();

            if(answer == "1." || answer == "1" || answer.ToLower() == "tak")
            {
                this.again = true;
            }
            else if(answer == "2." || answer == "2" || answer.ToLower() == "nie")
            {
                this.again = false;
            }
            else
                ask_repeat();
        }

        public static int get_window_width()
        {
            return Console.WindowWidth;
        }

        public static int get_window_height()
        {
            return Console.WindowHeight;
        }

        struct Ekran
        {
            public ConsoleColor ConsoleBG
            {
                get { return Console.BackgroundColor; }
            }

            public int BufferHeight
            {
                get { return Console.BufferHeight; }
            }

            public bool CapsLockIsOn
            {
                get { return Console.CapsLock; }
            }

            public int CursorLeftPosition
            {
                get { return Console.CursorLeft; }
            }

            public int CursorSize
            {
                get { return Console.CursorSize; }
            }

            public int CursorTopPosition
            {
                get { return Console.CursorTop; }
            }

            public bool CursorIsVisible
            {
                get { return Console.CursorVisible; }
            }

            public ConsoleColor FontColor
            {
                get { return Console.ForegroundColor; }
            }

            public int MaxWindowHeight
            {
                get { return Console.LargestWindowHeight; }
            }

            public int MaxWindowWidth
            {
                get { return Console.LargestWindowWidth; }
            }

            public bool NumLock
            {
                get { return Console.NumberLock; }
            }

            public string TitleOfWindow
            {
                get { return Console.Title; }
            }

            public int WindowHeight
            {
                get { return Console.WindowHeight; }
            }

            public int WindowLeft
            {
                get { return Console.WindowLeft; }
            }

            public int WindowTop
            {
                get { return Console.WindowTop; }
            }

            public int WindowWidth
            {
                get { return Console.WindowWidth; }
            }
        }
    }
}