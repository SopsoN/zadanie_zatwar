using System;
using System.Collections;
using System.Data;
using System.Data.Common;

namespace zadanie_dwa
{   
    public class EventClass
    {    

        public delegate void DrawFigureEventHandler(double sideSize, double window_field);

        public event DrawFigureEventHandler otherDraw;
        
        public void DrawEventFigure(double sideSize, double window_field)
        {
            if(sideSize != 0)
            {
                if(otherDraw != null)
                {
                    otherDraw(sideSize, window_field);
                }
            }
            else
            {
                Console.WriteLine("Wielkosc boku = "+sideSize);
            }
        }

        public delegate void setNewPosToFigures(int posX, int posY);
        public event setNewPosToFigures setNewPos;
        
        public void giveNewPosToFigures(int posX, int posY)
        {
            if(setNewPos != null)
            {
                setNewPos(posX, posY);
            }
        }
    }
}