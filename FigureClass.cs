using System;
using System.Collections;
using System.Data;
using System.Data.Common;


namespace zadanie_dwa
{   
    public abstract class FigureClass
    {    
        private static int x = 0;
        private static int y = 0;
        public abstract void draw(out double field);

        public FigureClass() { }

        public int posX 
        { 
            set
            {
                if(value >= 0)
                    FigureClass.x = value;    
                else
                {
                    FigureClass.x = 0;
                    Console.WriteLine("Niepoprawna wartosc, ustawiono domyslna = "+FigureClass.x);
                }
            }
            
            get
            {
                return FigureClass.x;
            } 
        }
        public int posY 
        {  
            set
            {
                if(value >= 0)
                    FigureClass.y = value;    
                else
                {
                    FigureClass.y = 0;
                    Console.WriteLine("Niepoprawna wartosc, ustawiono domyslna = "+FigureClass.y);
                }
            }
            
            get
            {
                return FigureClass.x;
            } 
        }

        public void setPosXY(int posNewX, int posNewY)
        {
            this.posX = posNewX;
            this.posY = posNewY;
        }

        public void moveToX()
        {
            Console.CursorLeft = this.posX;
        }       
        public void moveToY()
        {
            Console.CursorTop = this.posY;
        }
        public void moveToXY()
        {
            Console.CursorLeft = this.posX;
            Console.CursorTop = this.posY;
        }
    }
}